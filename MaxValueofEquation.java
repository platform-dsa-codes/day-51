import java.util.ArrayDeque;
import java.util.Deque;

class Solution {
    public int findMaxValueOfEquation(int[][] points, int k) {
        Deque<int[]> deque = new ArrayDeque<>();
        int maxValue = Integer.MIN_VALUE;

        for (int[] point : points) {
            // Remove points which are out of the range k
            while (!deque.isEmpty() && point[0] - deque.peekFirst()[0] > k) {
                deque.pollFirst();
            }

            if (!deque.isEmpty()) {
                int[] top = deque.peekFirst();
                int equationValue = point[0] - top[0] + top[1] + point[1];
                maxValue = Math.max(maxValue, equationValue);
            }

            // Remove points with smaller slopes
            while (!deque.isEmpty() && point[1] - point[0] >= deque.peekLast()[1] - deque.peekLast()[0]) {
                deque.pollLast();
            }

            deque.offerLast(point);
        }

        return maxValue;
    }
}
